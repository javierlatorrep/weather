const loadingElement = document.querySelector('.js-weather-loading');
const currentElement = document.querySelector('.js-weather-current');

console.log('loadingElement: ', loadingElement);

loadingElement.classList.add('hidden');
currentElement.classList.remove('hidden');
